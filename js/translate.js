function translate(language) {
    $.ajax({
        url: "./js/languages.xml",
        dataType: "xml" ,
        success: function(xml) {
            $(xml).find('translation').each(function(){
                var id = $(this).attr('id');		
                var text = $(this).find(language).text();
		var elm = document.getElementById(id) || document.getElementsByClassName(id);
		if(elm.length){
		  $.each(elm, function(index, source) {
		    source.innerHTML = text;
		  });
		}else elm.innerHTML = text;		
            });

          $('[data-language="' + language + '"]').addClass('active');
          var langs = ['en', 'pt', 'es', 'zh'];
          for (i = 0; i < langs.length; i++) {
            if (language != langs[i]) {
              $('[data-language="' + langs[i] + '"]').removeClass('active');
            }
          }
        }
    });
}
