
pt: Um passeio pelos momentos que marcaram os 20 anos de história da comunidade, começando pelas tecnologias que possibilitaram sua existência.
en: A tour through the moments that marked the 20 years of community history, starting with the technologies that made possible its existence.

pt: Surge o UNIX
Em 1969, Ken Thompson e Dennis Ritchie, começaram a trabalhar no UNIX. Escrito inicialmente em assembler, logo foi reescrito em C,
uma linguagem criada por Ritchie e considerada de alto nível.
en: UNIX is born
In 1969, Ken Thompson and Dennis Ritchie started working on UNIX. Initially written in assembler, it was soon rewritten in C,
a language created by Ritchie and considered high level.

pt: C++ foi criada
Em 1979, Bjarne Stroustrup começou a desenvolver a "C com classes", que se tornaria mais tarde a C++. Na sua opinião,
era a única linguagem da época que permitia escrever programas que fossem ao mesmo tempo eficientes e elegantes. 
en: C++ was created
In 1979, Bjarne Stroustrup started developing "C with classes", which would later become the C ++. In your opinion,
it was the only language of the time that allowed to write programs that were at the same time efficient and elegant.
;;;;;;;;;;


pt: O começo do software livre
Em 1984, Richard Stallman começa a desenvolver o GNU (GNU is Not Unix), um sistema operacional totalmente livre baseado no Unix,
que era proprietário.
en: The beginning of free software
In 1984, Richard Stallman started developing the GNU (GNU is Not Unix), a completely free operating system based on Unix,
which was proprietary.

pt: Surge o kernel Linux
Em 1991, Linus Torvalds escreve o kernel Linux baseado no Minix, uma versão do Unix escrita por Andrew Tanenbaum.
O surgimento do Linux revolucionou a história do software livre e ajudou a popularizá-lo.
en: Kernel Linux arises
In 1991, Linus Torvalds created the Linux kernel based on Minix, a version of Unix written by Andrew Tanenbaum.
The emergence of Linux has revolutionized the history of free software and helped to popularize it.

;;;;;;;;;;


pt: Nascem as primeiras distros
Em 1993, começam a surgir as primeiras distribuições livres, que eram formadas pela junção do GNU com o Linux.
en: The first distros are born
In 1993, begin to emerge the first free distributions, which were formed by the junction of GNU and Linux.

pt: Qt foi criado
Em 1995, a empresa norueguesa Troll Tech criou o framework multiplataforma Qt, com o qual o KDE seria criado no ano seguinte.
O Qt se tornou a base das principais tecnologias do KDE nesses 20 anos.
en: Qt was created
In 1995, the Norwegian company Troll Tech created the cross-platform framework Qt, with which the KDE would be created in the
following year. Qt became the basis of the main KDE technologies in these 20 years.
;;;;;;;;;;;;;

pt: KDE foi anunciado
Em 1996, Matthias Ettrich anuncia a criação do Kool Desktop Environment (KDE), uma interface gráfica para sistemas Unix,
construída com Qt e C++ e voltada para o usuário final. O nome KDE era um trocadilho com o ambiente gráfico CDE,
que era proprietário na época.
en: KDE was announced
In 1996, Matthias Ettrich announced the creation of Kool Desktop Environment (KDE), a graphical interface for Unix systems,
built with Qt and C ++ and designed for the end user. KDE name was a pun on the graphic environment CDE,
which was proprietary at the time.

pt: KDE One Conference
Em 1997, cerca de 15 desenvolvedores do KDE se encontraram em Arnsberg, na Alemanha, para trabalhar no projeto e
discutir o seu futuro. Esse evento ficou conhecido como KDE One.
en: KDE One Conference
In 1997, about 15 KDE developers met in Arnsberg, Germany, to work on the project and discuss its future.
This event became known as KDE One.

;;;;;;;;;;;

pt: KDE Beta 1
A versão beta 1 do KDE foi lançada exatamente 12 meses após o anúncio do projeto. O texto de lançamento destacava que
o KDE não era um gerenciador de janelas, mas um ambiente integrado no qual o gerenciador de janelas era só mais uma parte.
en: KDE Beta 1
The beta 1 version of KDE was released exactly 12 months after the project announcement. The release text emphasized
that KDE was not a window manager, but an integrated environment in which the window manager was just another part.

pt: KDE e.V foi criado
Em 1997, foi criado em Berlim o KDE e.V, organização sem fins lucrativos que representa financeira e legalmente a comunidade KDE.
en: KDE e.V was created
In 1997, was created in Berlin the KDE e.V, the nonprofit organization that represents financially and legally the KDE community.

;;;;;;;;;;;;;;

pt: KDE Free Qt Foundation foi criada
KDE assinou com a Troll Tech, então dona do Qt, o contrato de fundação da KDE Free Qt Foundation, que garante a permanente
disponibilização do Qt em licença livre.
en: KDE Free Qt Foundation was created
KDE signed with Trolltech, then owner of Qt, the foundation agreement of the KDE Free Qt Foundation, which ensures the permanent
availability of Qt in free license.

pt: KDE 1 foi lançado
KDE lançou a primeira versão estável do seu ambiente gráfico em 1998, apresentando como destaques um framework de desenvolvimento
de aplicação, o KOM/OpenParts, e uma prévia de sua suíte office.
en: KDE 1 was released
KDE released the first stable version of its graphical environment in 1998, with highlights as an application development framework,
the KOM/OpenParts, and a preview of its office suite.

;;;;;;;;;;;

pt: Konqi
Em abril de 1999, um dragão é anunciado como o novo assistente animado para o KDE Help Center. Ele agradou tanto que acabou
substituindo o então mascote do projeto Kandalf a partir das versões 3.x.
en: Konqi
In April 1999, a dragon is announced as the new animated assistant to the KDE Help Center. It chamerd so much that it was
replaced the then project mascot, Kandalf, form the versions 3.x.

pt: KDE Two Conference
Em outubro de 1999, aconteceu o segundo encontro do desenvolvedores do KDE, em Erlangen, na Alemanha.
en: KDE Two Conference
In October 1999, it took place the second meeting of the KDE developers in Erlangen, Germany.

pt: "KDE Desktop"
A partir da versão beta 1 do KDE 2 é possível perceber uma mudança de nomenclatura do projeto. Os anúncios que antes se referiam
ao projeto como "K Desktop Environment", passam a se referir apenas como "KDE Desktop".
en: "KDE Desktop"
From the beta 1 version of KDE 2 it is possible to perceive a project naming change. The releases that once referred to the project
as "K Desktop Environment", began refering only as "KDE Desktop".

pt: KDE Three Beta Conference
Em julho de 2000, ocorreu o terceiro encontro (beta) de desenvolvedores do KDE, em Trysil, na Noruega.
en: KDE Three Beta Conference
In July 2000, the third meeting (beta) of KDE developers occurred in Trysil, Norway.

pt: KDE 2 foi lançado
KDE lança sua segunda versão, apresentando como principais novidades o Konqueror, navegador web e gerenciador de arquivos;
e a suíte de escritório KOffice. O KDE teve seu código quase todo reescrito para essa 2 versão.
en: KDE 2 was released
KDE released its second version, featuring as main news Konqueror web browser and file manager; and the office suite KOffice.
KDE had its code almost entirely  rewritten for this second version.

pt: "KDE Project"
A partir do anúncio de lançamento da versão 2.1.2 há também uma mudança em relação à nomenclatura. 
Os anúncios passam a se referir ao KDE como "KDE Project".
en: "KDE Project"
From the release announcement of version 2.1.2 there is also a change of the nomenclature.
The announcements began refering to KDE as "KDE Project."

pt: KDE Women
Em março de 2001,foi anunciada a criação do grupo de mulheres da comunidade. O KDE Women tinha como objetivo ajudar a
aumentar o número de mulheres nas comunidades de software livre, em especial na KDE.
en: KDE Women
In March 2001, the creation of community women's group was announced. The KDE Women aimed to help increase the number
of women in free software communities, particularly in KDE.

pt: KDE Three Meeting
Em março de 2002, cerca de 25 desenvolvedores se reuniram no terceiro encontro do KDE, em Nurembergue, Alemanha.
O KDE 3 estava prestes a ser lançado e o código do KDE 2 precisava ser migrado para a nova biblioteca Qt 3.
en: KDE Three Meeting
In March 2002, about 25 developers gathered together in the third KDE meeting in Nuremberg, Germany. The KDE 3
was about to be released and the KDE 2 code needed to be migrated to the new library Qt 3.

pt: KDE 3
KDE lançou sua terceira versão, apresentando como adições importantes um novo framework de impressão, o KDEPrint;
a tradução do projeto para 50 línguas; e um pacote com aplicações educativas, mantidas pelo KDE Edutainment Project.
en: KDE 3
KDE released its third version, showing as important additions a new print framework, KDEPrint; the translation of
project for 50 languages; and a package of educational applications, maintained by the KDE Edutainment Project.

pt: KDE 3.1
Na versão 3.1 a comunidade apresentou o KDE com um novo visual, um novo tema para os widgets, chamado Keramik, e o Crystal como
tema padrão para os ícones.
en: KDE 3.1
In version 3.1 the community presented the KDE with a new look, a new theme for widgets, called Keramik, and Crystal
as default theme for the icons.

pt: Kastle
Em Agosto de 2003, cerca de 100 contribuidores do KDE vindos de vários países do mundo se reuniram em um castelo na República Tcheca.
O evento foi chamado de Kastle e foi o precursor do Akademy, o evento que se tornaria o encontro anual internacional da comunidade. 
en: Kastle
In August 2003, about 100 contributors of KDE from various countries of the world gathered in a castle in the Czech Republic.
The event was called Kastle and was the forerunner of Akademy, the event that would become the international annual meeting of the community.

pt: KDE 3.2
Em fevereiro de 2004, o KDE lançou sua versão 3.2 estreando um novo visual com novos ícones e um novo tema para widgets, o Plastik,
além de novas aplicações como o player de música JuK, o mensageiro instântaneo Kopete, e o serviço de armazenamento seguro de dados
e senhas, o KWallet.
en: KDE 3.2
In February 2004, KDE released its version 3.2 debuting a new look with new icons and a new theme for widgets, the Plastik,
as well as new applications such as JuK music player, the messenger instantaneous Kopete, and the secure storage service of data and
passwords, KWallet.

pt: Akademy 2004
Em Agosto de 2004, aconteceu o primeiro encontro internacional da comunidade. O evento aconteceu em Ludwigsburg, na Alemanha,
e inaugurou a série de eventos internacionais chamados de "aKademy", que acontecem anualmente desde então. O evento ganhou esse
apelido porque aconteceu na "Filmakademie", escola de cinema da cidade.
en: Akademy 2004
In August 2004,took place the first international meeting of the community. The event was held in Ludwigsburg, Germany, and launched a
series of international events called "aKademy" which take place annually since then. The event got its name because it happened
in the "Filmakademie" city film school.

pt: KDE 3.5
KDE 3.5 foi lançado. Essa versão apresentou várias novidades, dentre elas, o SuperKaramba, uma ferramenta que permitia personalizar sua área
de trabalho com "applets"; os players Amarok e o Kaffeine; e o gravador de mídias K3B.
en: KDE 3.5
KDE 3.5 was released. This version introduced several new features, among them, the SuperKaramba, a tool that allowed customize your desktop
with "applets"; the Amarok and Kaffeine players; and the media burner K3B.

pt: Primeiro Akademy-es
Em março de 2006, aconteceu em Barcelona o primeiro encontro de contribuidores espanhóis do KDE. Após essa edição o evento passou a ser anual.
en: First Akademy-es
In March 2006, it took place in Barcelona the first meeting of Spanish KDE contributors. After this edition, the event has become annual.

pt: Guademy
Em março de 2007, vários contribuidores do KDE e do Gnome se encontraram em A Coruña, na Espanha, num evento que pretendia estabelecer uma
colaboração entre os projetos. O evento ficou conhecido como Guademy, uma mistura de Guadec, nome dado ao evento do Gnome, com Akademy,
nome do evento do KDE.
en: Guademy
In March 2007, several contributors of KDE and Gnome met in A Coruña, Spain, an event which sought to establish a collaboration between the
two projects. The event became known as Guademy, a mixture of Guadec, the name given to the Gnome event with Akademy, KDE event name.

pt: KDE 4 Alfa 1
Em maio de 2007 foi anunciada a versão alfa 1 do KDE 4, codinome "Knut". Esse anúncio apresentou um ambiente de trabalho completamente novo,
com um novo tema, o Oxygen, novas aplicações como o Okular e o Dolphin, e um novo desktop shell, o Plasma.
en: KDE 4 Alfa 1
In May 2007 it was announced the alpha 1 version of KDE 4, codenamed "Knut". This announcement showed a completely new desktop,
with a new theme, Oxygen, new applications like Okular and Dolphin, and a new desktop shell, Plasma.

pt: KDE 4 Development Platform
Em Outubro de 2007, o KDE anuncia o release candidate da sua plataforma de desenvolvimento, composta das bibliotecas base e as ferramentas
para desenvolver as aplicações KDE.
en: KDE 4 Development Platform
In October 2007, KDE announced the release candidate of its development platform consisting of basic libraries and tools to develop KDE
applications.

pt: KDE 4
Em 2008 a comunidade anunciou o revolucionário KDE 4. Além do impacto visual causado pelo novo tema padrão, Oxygen, e pela nova interface
desktop, o Plasma; o KDE 4 também inovou apresentando o leitor de PDF Okular, o gerenciador de arquivos Dolphin,
além do KWin com suporte a efeitos gráficos. Os novos frameworks  Phonon e Solid representaram a reestruturação feita também em suas bibliotecas.
en: KDE 4
In 2008, the community announced the revolutionary KDE 4. In addition to the visual impact of the new deafult theme, Oxygen, and the
new desktop interface, the Plasma; KDE 4 also innovated by presenting the PDF reader Okular, the Dolphin file manager, as well KWin
supporting graphics effects. The new frameworks Phonon and Solid represented the restructuring also made in its libraries.

pt: "KDE Community"
A partir do anúncio da versão 4.1 já era visível a tendência de se referir ao KDE como "comunidade" e não mais apenas como um "projeto".
Essa mudança foi reconhecida e consolidada no anúncio de rebranding do ano seguinte.
en: "KDE Community"
From the announcement of 4.1 version it was already visible a tendency to refer to KDE as "community" and not just as a "project".
This change was recognized and affirmed in the rebranding announcement of the following year.

pt: Primeiro Camp KDE
Em janeiro de 2009, aconteceu a primeira edição do Camp KDE, o primeiro evento do KDE nas Américas. O evento aconteceu em Negril, na Jamaica.
Depois disso aconteceram apenas mais duas edições do evento, ambas nos Estados Unidos, uma em 2010, em San Diego, e outra em 2011, em São Francisco.
en: First Camp KDE
In January 2009, it took place the first edition of Camp KDE, the first KDE event in the Americas. The event took place in Negril, Jamaica.
After that happened just two events, both in the United States, one in 2010 in San Diego, and another in 2011 in San Francisco.

pt: Gran Canaria Desktop Summit
Em julho de 2009, em Gran Canaria,na Espanha, aconteceu o primeiro Desktop Summit, uma reunião das comunidades KDE e Gnome. O Akademy de
2009 foi realizado junto com esse evento.
en: Gran Canaria Desktop Summit
In July 2009, in Gran Canaria, Spain, it took place the first Desktop Summit, a meeting of the KDE and Gnome communities. The Akademy 2009
was held with this event.

pt: 1 milhão de commits
A comunidade atingiu a marca de 1 milhão de commits. Em janeiro de 2006 eram 500 mil, em dezembro de 2007 eram 750 mil, e apenas 19 meses
depois as contribuiçõe atingiram a marca de 1 milhão. O aumento dessas contribuições coincide com o lançamento do inovador KDE 4.
en: 1 million of commits
The community reached the mark of 1 million commits. In January 2006 was 500,000, in December 2007 were 750,000, and only 19 months
later contributions reached the 1 million mark. The increase in these contributions coincides with the launch of innovative KDE 4.

pt: Primeiro Randa Meetings
Em setembro de 2009, ocorreu o primeiro de uma série de eventos conhecidos como Randa Meetings, em Randa, nos Alpes Suiços. O evento
reuniu vários sprints de diversos projetos da comunidade. Desde então os Randa Meetings acontecem anualmente. 
en: First Randa Meetings
In September 2009, it took place the first of a series of events known as Randa Meetings in Randa, in the Swiss Alps. The event brought
together several sprints of various community projects. Since then Randa Meetings take place annually.

pt: Rebranding
Em novembro de 2009, a comunidade anunciou mudanças na sua marca. "KDE" não diz mais respeito a um desktop, agora representa tanto
a comunidade quanto um guarda-chuva de projetos mantidos por ela. O nome "K Desktop Environment" foi substituido apenas por "KDE",
pois se tornou ambiguo e obsoleto.
en: Rebranding
In November 2009, the community announced changes in its brand. "KDE" it is no longer about a desktop, now represents both the
community and a project umbrella supported by it. The name "K Desktop Environment" was replaced only by "KDE" because it has become
ambiguous and obsolete.

pt: KDE Software Compilation
A partir da versão 4.3.4, os anúncios começaram a se referir ao conjunto de produtos da comunidade como KDE Software Compilation (KDE SC).
Essa tendência atualmente foi abandonada.
en: KDE Software Compilation
From version 4.3.4, the announcements began to refer to the whole community products as KDE Software Compilation (KDE SC).
Currently this trend is abandoned.

pt: KDE SC 4.5
Em agosto de 2010 a comunidade anunciou a versão 4.5 de seus produtos: Development Platform, Aplications e Plasma Workspaces.
Cada um deles ganhou um texto de anúncio separado. Um dos destaques dessa versão foi a interface do Plasma para netbooks,
anunciada na versão 4.4.
en: KDE SC 4.5
In August 2010 the community announced the version 4.5 of its products: Development Platform, Aplications and Plasma Workspaces. 
Each of them began to have a separate release announcement. One of the highlights of this version was the Plasma interface for netbooks,
announced in version 4.4.

pt: Akademy-Br
Em abril de 2010 aconteceu o primeiro encontro de contribuidores brasileiros da comunidade KDE. O evento aconteceu em Salvador,
Bahia, e foi a única edição brasileira. A partir de 2012 o evento expandiu e se transformou em latino-americano.
en: Akademy-Br
In April 2010 took place the first meeting of Brazilian contributors of the KDE community. The event was held in Salvador, Bahia,
and was the only Brazilian edition. From 2012 the event expanded and turned into Latin American.

pt: Join the Game
Em junho de 2010, o KDE e.V anunciou o programa "Join the Game", que visa incentivar o apoio financeiro à comunidade através do sistema
de associação individual. Ao participar do programa você se torna sócio do KDE e.V, contribuindo com um valor anual e podendo participar
das reuniões anuais da organização.
en: Join the Game
In June 2010, the KDE e.V announced the program "Join the Game", which aims to encourage financial support to the community through
individual membership system. By participating in the program you become a member of the KDE e.V, contributing to an annual amount
and being able participate in the organization's annual meetings.

pt: Calligra Suite
Em dezembro de 2010 a comunidade anuncia o Calligra Suite, um fork da suíte KOffice, que era a suite padrão do projeto desde o seu início.
en: Calligra Suite
In December 2010 the community announces the Calligra Suite, a fork of the KOffice suite, which was the default suite of the project since its begin.

pt: Primeira Conf KDE India
Em março de 2011 aconteceu a primeira conferência da comunidade KDE e Qt na Índia, em Bengaluru. Desde então o evento tem acontecido anualmente.
en: First Conf KDE India
In March 2011 it took place the first conference of the KDE and Qt community in India in Bengaluru. Since then the event has taken place annually.

pt: Desktop Summit 2011
Em agosto de 2011 aconteceu maia um encontro internacional comunidades KDE e Gnome, em Berlim, na Alemanha. Desde então não houve mais
nenhum evento desse tipo.
en: Desktop Summit 2011
In August 2011 took place another international meeting of communities KDE and Gnome in Berlin, Germany. Since then there has been no
other such event.

pt: Plasma Active
A comunidade lançou a primeira versão de sua interface para dispositivos móveis, o Plasma Active. Após chegar na sua quarta versão,
o Plasma Active foi substituido pelo Plasma Mobile.
en: Plasma Active
The community released the first version of its interface for mobile devices, Plasma Active. After reaching in its fourth version,
Plasma Active was replaced by Plasma Mobile.

pt: Primeiro LaKademy
Em abril de 2012 aconteceu o primeiro encontro de colaboradores do KDE na América Latina, o LaKademy. O evento aconteceu em Porto Alegre, Brasil.
A segunda edição aconteceu em 2014, em São Paulo, e desde então tem sido um evento anual. Todas as edições realizadas até agora foram no Brasil,
onde se concentra a maior quantidade de contribuidores da comunidade latino-americana.
en: Primeiro LaKademy
In April 2012 took place the first meeting of the KDE contributors in Latin America, LaKademy. The event took place in Porto Alegre, Brazil.
The second edition took place in 2014 in São Paulo, and since then has been an annual event. All editions held so far were in Brazil,
where concentrates the highest number of contributors of the Latin American community.

pt: KDE Manifesto
Lançado o KDE Manifesto, um documento que apresentou os benefícios e compromissos de um projeto KDE, assim como os principais valores que
norteiam a comunidade: Open Governance, Software livre, Inclusão, Inovação, A Propriedade Coletiva e Foco no usuário final.
en: KDE Manifesto
KDE Manifesto was released, a document that presented the benefits and obligations of a KDE project as well as the core values
that guide the community: Open Governance, Free Software, Inclusivity, Innovation, Common Ownership, and End-User Focus.

pt: Novo Konqi
Em dezembro de 2012 a comunidade lançou um concurso para criar um novo mascote usando o Krita. O vencedor do concurso foi Tyson Tan,
que criou novos visuais para o Konqi e a Katie.
en: New Konqi
In December 2012 the community launched a competition to create a new mascot using Krita. The competition winner was Tyson Tan,
who created new looks for the Konqi and Katie.

pt: Mudança no ciclo de lançamentos
Em setembro de 2013 a comunidade anunciou mudanças no ciclo de lançamentos de seus produtos. Cada um deles, Workspaces,
Applications e Platform, passaram a ter lançamentos separados. A mudança já era um reflexo da reestruturação de suas tecnologias
rumo à próxima geração de seus produtos, que seria lançada no ano seguinte.
en: Change in the release cycle
In September 2013 the community announced changes in the release cycle of its products. Each of them, Workspaces, Applications and Platform,
now have separate releases. The change was already a reflection of the restructuring of its technologies into the next generation of their
products, which would be released the following year.

pt: Frameworks 5
Lançada a primeira versão estável do Frameworks 5 (KF5), o sucessor do KDE Platform 4. Essa nova geração de bibliotecas do KDE baseadas no Qt 5
tornou a plataforma de desenvolvimento do KDE mais modular e facilitou o desenvolvimento multi-plataforma.
en: Frameworks 5
Released the first stable version of Frameworks 5 (KF5), the successor of KDE Platform 4. This new generation of the KDE libraries based on Qt 5
has made the KDE development platform more modular and facilitated cross-platform development.

pt: Plasma 5
Lançada a primeira versão estável do Plasma 5. Essa nova geração do Plasma apresenta um novo tema, o Breeze, além de uma plataforma gráfica nova,
acelerada por hardware e com foco em uma cena gráfica de OpenGL(ES). Essa versão do Plasma usa como base o Qt 5 e o Frameworks 5.
en: Plasma 5
Released the first stable version of Plasma 5. This new generation of Plasma has a new theme, Breeze, and a new graphics platform,hardware
accelerated and focused on a graphic scene of OpenGL(ES). This version of the Plasma uses as base the Qt 5 and Frameworks 5.

pt: GCompris se junta ao KDE
Em dezembro de 2014, a suite de softwares educacionais GCompris se junta à incubadora de projetos da comunidade KDE. Bruno Coudoin,
que criou o projeto em 2000, decidiu reescrevê-lo em Qt Quick para facilitar o seu uso em plataformas móveis. Ele era originalmente escrito em GTK+.
en: GCompris joins the KDE
In December 2014, the educational software suite GCompris joins the project incubator of KDE community. Bruno Coudoin, who created the project in 2000,
decided to rewrite it in Qt Quick to facilitate its use on mobile platforms. It was originally written in GTK+.

pt: Plasma Mobile
A comunidade anunciou o Plasma Mobile, uma interface para smartphones que usa as tecnologias Qt, Frameworks 5 e o Plasma Shell.
en: Plasma Mobile
The community announced the Plasma Mobile, an interface for smartphones that uses Qt, Frameworks 5 and Plasma Shell technologies.

pt: Plasma no Wayland
Disponibilizada para download a primeira live image do Plasma rodando no Wayland. Desde 2011 a comunidade trabalha no suporte do Wayland pelo KWin,
o Compositor do Plasma e o Window Manager.
en: Plasma on Wayland
Made available for download the first live image of Plasma running on Wayland. Since 2011 the community works in support of Wayland by KWin,
the Compositor of Plasma, and the Window Manager.

pt: Plasma 5.5
A versão 5.5 foi anunciada com várias novidades: novos ícones adicionados ao tema Breeze, suporte ao OpenGL ES no KWin, progresso no suporte
ao Wayland, nova fonte padrão (Noto), novo design.
en: Plasma 5.5
Version 5.5 was announced with several new features: new icons added to the theme Breeze, support for OpenGL ES in KWin, progress in support
Wayland, new default font (Noto), new design.

pt: KDE Neon
A comunidade anunciou a inclusão de mais um projeto em sua incubadora, a distro KDE Neon, baseada na Kubuntu. É a primeira vez que uma distro
se torna parte da comunidade oficialmente.
en: KDE Neon
The community announced the inclusion of another project in its incubator, the distro KDE Neon, based on Kubuntu. It is the first time a distro
becomes part of the community officially.

pt: Akademy 2016 junto com a QtCon
O Akademy 2016 foi anunciado como parte da QtCon, que acontecerá em setembro, em Berlim, Alemanha. O evento reunirá os encontros das comunidades Qt,
FSFE, VideoLAN e KDAB. Essa reunião de comunidades será histórica e celebrará os 20 anos do KDE, 20 anos da FSFE e 15 anos do VideoLAN.
en: Akademy 2016 part of QtCon
The Akademy 2016 was announced as part of QtCon that to be held in September in Berlin, Germany. The event will bring together the meetings of Qt,
FSFE, VideoLAN and KDAB communities. This meeting of communities will be historical and it will celebrate 20 years of KDE, 20 years of FSFE,
and 15 years of VideoLAN.

pt: Kirigami UI
Lançado o Kirigami, um conjunto de componentes QML para desenvolvimento de aplicações baseadas em Qt, para dispositivos móveis ou desktop.
en: Kirigami UI
Released Kirigami, a set of QML components to develop applications based on Qt for mobile or desktop devices.

pt: KDE celebrará 20 anos
Em 14 de outubro de 2016 o KDE completará 20 anos. O projeto que começou como um ambiente desktop para sistemas Unix, hoje é uma comunidade
incubadora de ideias e projetos que vão muito além de tecnologias para desktop. 
en: KDE will celebrate 20 years
On October 14, KDE will celebrate 20 years. The project that started as a desktop environment for Unix systems, today is a community that
incubates ideas and projects which go far beyond desktop technologies.



